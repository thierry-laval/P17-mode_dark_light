# ![left 100%](https://raw.githubusercontent.com/thierry-laval/archives/master/images/logo-portfolio.png)

### Auteur

👤**Thierry LAVAL** [🇫🇷 Contactez moi 🇬🇧](<contact@thierrylaval.dev>)

* Github: [@Thierry Laval](https://github.com/thierry-laval)
* LinkedIn: [@Thierry Laval](https://www.linkedin.com/in/thierry-laval)

***

## 📎 Projet 17 - Création d'un mode Dark/Night en css

![left 100%](dark_light.jpg?raw=true)
_`Début du projet le 20/04/2020`_

Le projet 17 consiste à construire un bouton pour afficher le mode dark/night sur une page web.

* Création d'une page html avec un ID, une class et un script.
* Création du style en css.

***

### Utilisé dans ce projet

| Languages       | et Applications    |
| :-------------: |:--------------:    |
| HTML5           | Visual Studio Code |
| CSS3            | Git/GitHub         |
| Javascript      |                    |

***

&hearts;&nbsp;&nbsp;&nbsp;&nbsp;Love Markdown
